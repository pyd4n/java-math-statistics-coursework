package polytech.math.statistics.coursework.chisquare;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;
import polytech.math.statistics.coursework.chisquare.component.ChiSquareTableRow;
import polytech.math.statistics.coursework.grouping.component.Interval;
import polytech.math.statistics.coursework.io.NumberedValue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class ChiSquareTest extends BaseUnitTest {
    private final List<ChiSquareTableRow> expectedRows = List.of(
            // 1
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(1)
                            .withStart(BigDecimal.valueOf(717.9))
                            .withEnd(BigDecimal.valueOf(751.9))
                            .withMiddle(BigDecimal.valueOf((717.9 + 751.9) / 2))
                            .withFrequency(6)
                            .withIncidence(BigDecimal.valueOf(0.005))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.005))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(-2.07))
                    .withProbability(BigDecimal.valueOf(0.037))
                    .withMathExpectance(BigDecimal.valueOf(7.3303))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(-1.3303))
                    .withPsiSquare(BigDecimal.valueOf(0.2414))
                    .build(),

            // 2
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(3)
                            .withStart(BigDecimal.valueOf(751.9))
                            .withEnd(BigDecimal.valueOf(768.9))
                            .withMiddle(BigDecimal.valueOf(760.4))
                            .withFrequency(7)
                            .withIncidence(BigDecimal.valueOf(0.035))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.065))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(-1.56))
                    .withProbability(BigDecimal.valueOf(0.054))
                    .withMathExpectance(BigDecimal.valueOf(10.8374))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(-3.8374))
                    .withPsiSquare(BigDecimal.valueOf(1.3588))
                    .build(),

            // 3
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(4)
                            .withStart(BigDecimal.valueOf(768.9))
                            .withEnd(BigDecimal.valueOf(785.9))
                            .withMiddle(BigDecimal.valueOf(777.4))
                            .withFrequency(12)
                            .withIncidence(BigDecimal.valueOf(0.06))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.125))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(-1.05))
                    .withProbability(BigDecimal.valueOf(0.094))
                    .withMathExpectance(BigDecimal.valueOf(18.7789))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(-6.7789))
                    .withPsiSquare(BigDecimal.valueOf(2.4471))
                    .build(),

            // 4
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(5)
                            .withStart(BigDecimal.valueOf(785.9))
                            .withEnd(BigDecimal.valueOf(802.9))
                            .withMiddle(BigDecimal.valueOf(794.4))
                            .withFrequency(36)
                            .withIncidence(BigDecimal.valueOf(0.18))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.305))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(-0.54))
                    .withProbability(BigDecimal.valueOf(0.136))
                    .withMathExpectance(BigDecimal.valueOf(27.1872))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(8.8128))
                    .withPsiSquare(BigDecimal.valueOf(2.8567))
                    .build(),

            // 5
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(6)
                            .withStart(BigDecimal.valueOf(802.9))
                            .withEnd(BigDecimal.valueOf(819.9))
                            .withMiddle(BigDecimal.valueOf(811.4))
                            .withFrequency(34)
                            .withIncidence(BigDecimal.valueOf(0.17))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.475))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(-0.03))
                    .withProbability(BigDecimal.valueOf(0.164))
                    .withMathExpectance(BigDecimal.valueOf(32.8864))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(1.1136))
                    .withPsiSquare(BigDecimal.valueOf(0.0377))
                    .build(),

            // 6
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(7)
                            .withStart(BigDecimal.valueOf(819.9))
                            .withEnd(BigDecimal.valueOf(836.9))
                            .withMiddle(BigDecimal.valueOf(828.4))
                            .withFrequency(38)
                            .withIncidence(BigDecimal.valueOf(0.19))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.665))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(0.48))
                    .withProbability(BigDecimal.valueOf(0.166))
                    .withMathExpectance(BigDecimal.valueOf(33.2376))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(4.7624))
                    .withPsiSquare(BigDecimal.valueOf(0.6824))
                    .build(),

            // 7
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(8)
                            .withStart(BigDecimal.valueOf(836.9))
                            .withEnd(BigDecimal.valueOf(853.9))
                            .withMiddle(BigDecimal.valueOf(845.4))
                            .withFrequency(32)
                            .withIncidence(BigDecimal.valueOf(0.16))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.825))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(0.99))
                    .withProbability(BigDecimal.valueOf(0.140))
                    .withMathExpectance(BigDecimal.valueOf(28.0676))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(3.9324))
                    .withPsiSquare(BigDecimal.valueOf(0.5509))
                    .build(),

            // 8
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(9)
                            .withStart(BigDecimal.valueOf(853.9))
                            .withEnd(BigDecimal.valueOf(870.9))
                            .withMiddle(BigDecimal.valueOf(862.4))
                            .withFrequency(25)
                            .withIncidence(BigDecimal.valueOf(0.125))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.95))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(1.50))
                    .withProbability(BigDecimal.valueOf(0.099))
                    .withMathExpectance(BigDecimal.valueOf(19.8034))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(5.1966))
                    .withPsiSquare(BigDecimal.valueOf(1.3637))
                    .build(),

            // 9
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(10)
                            .withStart(BigDecimal.valueOf(870.9))
                            .withEnd(BigDecimal.valueOf(887.9))
                            .withMiddle(BigDecimal.valueOf(879.4))
                            .withFrequency(5)
                            .withIncidence(BigDecimal.valueOf(0.025))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.975))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(2.01))
                    .withProbability(BigDecimal.valueOf(0.058))
                    .withMathExpectance(BigDecimal.valueOf(11.6741))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(-6.6741))
                    .withPsiSquare(BigDecimal.valueOf(3.8156))
                    .build(),

            // 10
            ChiSquareTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(11)
                            .withStart(BigDecimal.valueOf(887.9))
                            .withEnd(BigDecimal.valueOf(904.9))
                            .withMiddle(BigDecimal.valueOf(896.4))
                            .withFrequency(5)
                            .withIncidence(BigDecimal.valueOf(0.025))
                            .withAccumulatedIncidence(BigDecimal.valueOf(1))
                            .build())
                    .withDeviationFraction(BigDecimal.valueOf(2.52))
                    .withProbability(BigDecimal.valueOf(0.029))
                    .withMathExpectance(BigDecimal.valueOf(5.7497))
                    .withFrequencyExpectanceDifference(BigDecimal.valueOf(-0.7497))
                    .withPsiSquare(BigDecimal.valueOf(0.0978))
                    .build());

    @Test
    @DisplayName("Тест расчета хихихи квадрата на примере таблицы из отчета")
    public void exampleDataTest() {
        List<BigDecimal> allNumbers = fileDataReader
                .readNumbers(TEST_DATA)
                .stream().map(NumberedValue::getValue).toList();

        BigDecimal allDeviation = dispersionCalculator
                .calculateBiasedEstimation(allNumbers);

        List<Interval> intervals = getIntervals(TEST_VARIATION_SERIES);

        List<BigDecimal> secondNumbers = getNumbers(SECOND_TEST_VARIATION_SERIES);
        var localMathExpectation = mathExpectationTrustIntervalCalculator
                .calculateAverage(secondNumbers).setScale(3, RoundingMode.HALF_UP);
        var localDeviation = mathExpectationTrustIntervalCalculator
                .calculateDispersion(secondNumbers).setScale(1, RoundingMode.HALF_UP);
        var table = chiSquareCalculator.calculateChiSquareTable(intervals, localMathExpectation, localDeviation,
                allDeviation);
        var rows = table.getRows();
        Assertions.assertEquals(expectedRows.size(), rows.size());
        for (int i = 0; i < expectedRows.size(); ++i) {
            var currentRow = rows.get(i);
            var expectedRow = expectedRows.get(i);

            Assertions.assertEquals(expectedRow.getDeviationFraction().setScale(2, RoundingMode.HALF_UP).doubleValue(),
                    currentRow.getDeviationFraction().setScale(2, RoundingMode.HALF_UP).doubleValue());

            Assertions.assertEquals(expectedRow.getProbability().setScale(3, RoundingMode.HALF_UP).doubleValue(),
                    currentRow.getProbability().setScale(3, RoundingMode.HALF_UP).doubleValue());

            Assertions.assertEquals(expectedRow.getMathExpectance().setScale(4, RoundingMode.HALF_UP).doubleValue(),
                    currentRow.getMathExpectance().setScale(4, RoundingMode.HALF_UP).doubleValue());

            Assertions.assertEquals(expectedRow.getInterval().getFrequency(),
                    currentRow.getInterval().getFrequency());

            Assertions.assertEquals(
                    expectedRow.getFrequencyExpectanceDifference().setScale(4, RoundingMode.HALF_UP).doubleValue(),
                    currentRow.getFrequencyExpectanceDifference().setScale(4, RoundingMode.HALF_UP).doubleValue());

            Assertions.assertEquals(expectedRow.getPsiSquare().setScale(4, RoundingMode.HALF_UP).doubleValue(),
                    currentRow.getPsiSquare().setScale(4, RoundingMode.HALF_UP).doubleValue());
        }
    }
}

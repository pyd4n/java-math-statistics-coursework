package polytech.math.statistics.coursework.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.util.List;

/**
 * Класс для тестирования чтения данных из файлаю.
 */
public class FileDataReaderTest extends BaseUnitTest {
    /**
     * Тест чтения данных из примера отчета.
     */
    @Test
    public void exampleDataReadTest() {
        List<NumberedValue> numbers = fileDataReader
            .readNumbers("test-data/unsorted_example_report_data.txt");
        Assertions.assertEquals(200, numbers.size());
    }
}

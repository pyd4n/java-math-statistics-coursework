package polytech.math.statistics.coursework.calculation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;
import polytech.math.statistics.coursework.io.NumberedValue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для проверки вычисления смещенной и несмещенной
 * оценки дисперсии по заданной выборке.
 */
class DispersionCalculatorTest extends BaseUnitTest {
    @Test
    @DisplayName("Тест расчета оценки дисперсии на тестовых данных из отчета")
    public void exampleDataTest() {
        List<BigDecimal> numbers = fileDataReader
            .readNumbers(TEST_DATA)
            .stream().map(NumberedValue::getValue).toList();

        BigDecimal biasedEstimation = dispersionCalculator
            .calculateBiasedEstimation(numbers);
        BigDecimal unBiasedEstimation = dispersionCalculator
            .calculateUnbiasedEstimation(numbers);

        Assertions.assertEquals(BigDecimal.valueOf(33.28), biasedEstimation
            .setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(33.36), unBiasedEstimation
            .setScale(2, RoundingMode.HALF_UP));
    }
}

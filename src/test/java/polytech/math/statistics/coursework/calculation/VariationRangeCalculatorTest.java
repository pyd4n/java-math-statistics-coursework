package polytech.math.statistics.coursework.calculation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Тест расчета размаха варьирования.
 */
public class VariationRangeCalculatorTest extends BaseUnitTest  {
    @Test
    @DisplayName("Тест расчета размаха варьирования на тестовых данных из отчета")
    public void exampleDataTest() {
        List<BigDecimal> numbers = getNumbers(TEST_VARIATION_SERIES);

        BigDecimal variationRange = variationRangeCalculator
            .calculateVariationRange(numbers);

        Assertions.assertEquals(BigDecimal.valueOf(177.8),
            variationRange.setScale(1, RoundingMode.HALF_UP));
    }
}

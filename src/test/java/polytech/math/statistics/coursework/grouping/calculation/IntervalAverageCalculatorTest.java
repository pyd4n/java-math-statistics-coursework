package polytech.math.statistics.coursework.grouping.calculation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Тест расчета среднего арифметического
 * по заданным интервалам
 */
public class IntervalAverageCalculatorTest extends BaseUnitTest {
    @Test
    @DisplayName("Тест расчета среднего арифметического по интервалам " +
        "на тестовых данных из отчета")
    public void exampleDataTest() {
        Assertions.assertEquals(BigDecimal.valueOf(821.26), intervalAverageCalculator
            .calculateIntervalAverage(getIntervals(TEST_VARIATION_SERIES)).setScale(2, RoundingMode.HALF_UP));
    }
}

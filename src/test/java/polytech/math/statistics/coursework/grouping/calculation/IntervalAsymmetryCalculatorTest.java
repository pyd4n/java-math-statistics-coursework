package polytech.math.statistics.coursework.grouping.calculation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;
import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Тест расчета асимметрии.
 */
public class IntervalAsymmetryCalculatorTest extends BaseUnitTest {
    @Test
    @DisplayName("Тест расчета асимметрии на тестовых данных из отчета")
    public void exampleDataTest() {
        List<Interval> intervals = getIntervals(TEST_VARIATION_SERIES);
        Assertions.assertEquals(BigDecimal.valueOf(-0.1617), intervalAsymmetryCalculator
            .calculateIntervalAsymmetry(intervals).setScale(4, RoundingMode.HALF_UP));
    }
}

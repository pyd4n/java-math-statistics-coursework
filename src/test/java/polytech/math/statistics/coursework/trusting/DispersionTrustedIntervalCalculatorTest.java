package polytech.math.statistics.coursework.trusting;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Тест для вычисления доверительного интервала для
 * дисперсии и среднего квадратического отклонения.
 */
public class DispersionTrustedIntervalCalculatorTest extends BaseUnitTest {
    @Test
    public void squaredBorderTest() {
        List<BigDecimal> numbers = getNumbers(SECOND_TEST_VARIATION_SERIES);

        // q = 5%, X1 = 8.9065, X2 = 32.8523
        Assertions.assertEquals(BigDecimal.valueOf(964.344), dispersionTrustedIntervalCalculator
            .calculateSquaredLeftBorder(numbers, BigDecimal.valueOf(32.8523))
            .setScale(3, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(3557.055), dispersionTrustedIntervalCalculator
            .calculateSquaredRightBorder(numbers, BigDecimal.valueOf(8.9065))
            .setScale(3, RoundingMode.HALF_UP));

        // q = 1%, X1 = 6.84397, X2 = 38.58226
        Assertions.assertEquals(BigDecimal.valueOf(821.126), dispersionTrustedIntervalCalculator
            .calculateSquaredLeftBorder(numbers, BigDecimal.valueOf(38.58226))
            .setScale(3, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(4629.025), dispersionTrustedIntervalCalculator
            .calculateSquaredRightBorder(numbers, BigDecimal.valueOf(6.84397))
            .setScale(3, RoundingMode.HALF_UP));

        // q = 10%, X1 = 10.1170, X2 = 30.1435
        Assertions.assertEquals(BigDecimal.valueOf(1051.003), dispersionTrustedIntervalCalculator
            .calculateSquaredLeftBorder(numbers, BigDecimal.valueOf(30.1435))
            .setScale(3, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(3131.453), dispersionTrustedIntervalCalculator
            .calculateSquaredRightBorder(numbers, BigDecimal.valueOf(10.1170))
            .setScale(3, RoundingMode.HALF_UP));
    }

    @Test
    public void notSquaredBorderTest() {
        List<BigDecimal> numbers = getNumbers(SECOND_TEST_VARIATION_SERIES);

        // q = 5%, X1 = 8.9065, X2 = 32.8523
        Assertions.assertEquals(BigDecimal.valueOf(31.054), dispersionTrustedIntervalCalculator
            .calculateLeftBorder(numbers, BigDecimal.valueOf(32.8523))
            .setScale(3, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(59.641), dispersionTrustedIntervalCalculator
            .calculateRightBorder(numbers, BigDecimal.valueOf(8.9065))
            .setScale(3, RoundingMode.HALF_UP));
    }
}

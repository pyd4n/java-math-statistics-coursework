package polytech.math.statistics.coursework.homogeneity;

import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Класс для проверки выборки на однородность
 * по критерию знаков.
 */
public class SignCriteriaCalculator {
    /**
     * Рассчитать число положительных и отрицательных знаков разностей.
     * @param numbers - выборка чисел
     * @param subsequenceSize - сколько чисел с начала и с конца взять
     * @return число положительных и отрицательных знаков разностей
     */
    public Pair<Integer, Integer> calculateSignCriteria(List<BigDecimal> numbers,
                                                        Integer subsequenceSize) {
        int positiveDifferenceCount = 0;
        int negativeDifferenceCount = 0;

        List<BigDecimal> numbersCopy = new ArrayList<>(numbers);
        List<BigDecimal> topNumbers = numbersCopy.stream().limit(subsequenceSize).toList();
        Collections.reverse(numbersCopy);
        List<BigDecimal> bottomNumbers = new ArrayList<>(numbersCopy.stream().limit(subsequenceSize).toList());
        Collections.reverse(bottomNumbers);

        for (int index = 0; index < subsequenceSize; index++) {
            if (topNumbers.get(index).compareTo(bottomNumbers.get(index)) > 0) {
                positiveDifferenceCount++;
            } else {
                negativeDifferenceCount++;
            }
        }

        return Pair.of(positiveDifferenceCount, negativeDifferenceCount);
    }
}

package polytech.math.statistics.coursework.kolmogorovsmirnov;

import java.math.BigDecimal;
import java.util.List;

public class KolmSmirnovAnalyzer {
    static private final List<BigDecimal> kolmTable = List.of(
            // 1% 5% 10%
            BigDecimal.valueOf(0.11525841), BigDecimal.valueOf(0.09616652), BigDecimal.valueOf(0.08626703));

    /**
     * 
     * @param kolmogorovValue - рассчитанное с помощью KolmSmirnovCalculator
     *                        значение критерия Колмогорова-Смирнова
     * @param q               - уровень значимости. В долях, то есть процент/100.
     *                        Допустимые значения 0.1 0.05 0.01
     * @return True если значение критерия противоречит гипотезе.
     *         False если по этому критерию противоречий нету.
     */
    public boolean isKolmValueContraveneNormDistribution(BigDecimal kolmogorovValue, BigDecimal q) {
        return kolmogorovValue.compareTo(getTableKolm(q)) >= 0;
    }

    public BigDecimal getTableKolm(BigDecimal q) {
        if (q.compareTo(BigDecimal.valueOf(0.01)) == 0)
            return kolmTable.get(0);
        if (q.compareTo(BigDecimal.valueOf(0.05)) == 0)
            return kolmTable.get(1);
        if (q.compareTo(BigDecimal.valueOf(0.1)) == 0)
            return kolmTable.get(2);
        return BigDecimal.valueOf(-1);
    }
}

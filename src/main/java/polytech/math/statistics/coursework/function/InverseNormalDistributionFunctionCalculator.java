package polytech.math.statistics.coursework.function;

import org.apache.commons.math3.distribution.NormalDistribution;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Обратная функция нормального распределения (даем значение - получаем аргумент).
 * По сути функция НОРМ.СТ.ОБР из Экселя.
 */
public class InverseNormalDistributionFunctionCalculator {

    /**
     * Получить значение параметра, при котором функция нормального распределения
     * принимает значение value
     *
     * @param value - значение функции нормального распределения
     * @return параметр
     */
    public BigDecimal getParameterByValue(BigDecimal value) {
        NormalDistribution standardNormalDistribution = new NormalDistribution(0.0, 2.0);
        return BigDecimal.valueOf(standardNormalDistribution
            .inverseCumulativeProbability(value.doubleValue()))
            .divide(BigDecimal.TWO, 10, RoundingMode.HALF_UP);
    }
}

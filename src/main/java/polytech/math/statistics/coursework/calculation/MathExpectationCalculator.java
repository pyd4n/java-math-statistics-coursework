package polytech.math.statistics.coursework.calculation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для расчета математического ожидания.
 */
public class MathExpectationCalculator {
    /**
     * Метод для расчета математического ожидания по заданной выборке.
     * @param numbers - выборка чисел
     * @return математическое ожидание выборки
     */
    public BigDecimal calculateMathExpectation(List<BigDecimal> numbers) {
        return numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add)
            .divide(BigDecimal.valueOf(numbers.size()), 10, RoundingMode.HALF_UP);
    }
}

package polytech.math.statistics.coursework.calculation;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для вычисления смещенной и несмещенной оценки дисперсии
 * по заданной выборке чисел.
 */
public class DispersionCalculator {
    private final MathExpectationCalculator mathExpectationCalculator =
        new MathExpectationCalculator();

    /**
     * Рассчитать смещенную оценку дисперсии по заданной выборке.
     *
     * @param numbers - выборка чисел
     * @return смещенная оценка дисперсии
     */
    public BigDecimal calculateBiasedEstimation(List<BigDecimal> numbers) {
        BigDecimal mathExpectation = mathExpectationCalculator
            .calculateMathExpectation(numbers);

        return numbers.stream().reduce(BigDecimal.ZERO, (acc, number) -> acc.add(number.pow(2)
                .subtract(mathExpectation.pow(2))))
            .divide(BigDecimal.valueOf(numbers.size()), 10, RoundingMode.HALF_UP)
            .sqrt(MathContext.DECIMAL128).setScale(10, RoundingMode.HALF_UP);
    }

    /**
     * Рассчитать несмещенную оценку дисперсии по заданной выборке.
     *
     * @param numbers - выборка чисел
     * @return смещенная оценка дисперсии
     */
    public BigDecimal calculateUnbiasedEstimation(List<BigDecimal> numbers) {
        return calculateBiasedEstimation(numbers).pow(2).multiply(BigDecimal.valueOf(numbers.size()))
            .divide(BigDecimal.valueOf(numbers.size() - 1), 10, RoundingMode.HALF_UP)
            .sqrt(MathContext.DECIMAL128).setScale(10, RoundingMode.HALF_UP);
    }
}

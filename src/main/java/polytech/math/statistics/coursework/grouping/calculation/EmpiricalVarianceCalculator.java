package polytech.math.statistics.coursework.grouping.calculation;

import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для расчета эмпирической дисперсии.
 */
public class EmpiricalVarianceCalculator {
    /**
     * Класс для расчета среднего арифметического
     * по заданным интервалам.
     */
    private final IntervalAverageCalculator intervalAverageCalculator =
        new IntervalAverageCalculator();

    /**
     * Рассчитать эмпирическую дисперсию.
     *
     * @param intervals - интервалы
     * @return эмпирическая дисперсия
     */
    public BigDecimal calculateEmpiricalVariance(List<Interval> intervals) {
        int elementsNumber = intervals.stream()
            .mapToInt(Interval::getFrequency).sum();
        BigDecimal average = intervalAverageCalculator
            .calculateIntervalAverage(intervals);

        BigDecimal sum = BigDecimal.ZERO;
        for (Interval interval : intervals) {
            BigDecimal difference = interval.getMiddle().subtract(average);
            sum = sum.add(BigDecimal.valueOf(interval.getFrequency())
                .multiply(difference.pow(2)));
        }

        sum = sum.divide(BigDecimal.valueOf(elementsNumber), 10, RoundingMode.HALF_UP);

        return sum.sqrt(MathContext.DECIMAL128).setScale(10, RoundingMode.HALF_UP);
    }
}

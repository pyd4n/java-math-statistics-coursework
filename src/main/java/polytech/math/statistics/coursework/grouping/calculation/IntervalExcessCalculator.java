package polytech.math.statistics.coursework.grouping.calculation;

import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для расчета эксцесса.
 */
public class IntervalExcessCalculator {
    /**
     * Класс для расчета среднего арифметического
     * по заданным интервалам.
     */
    private final IntervalAverageCalculator intervalAverageCalculator =
        new IntervalAverageCalculator();

    /**
     * Класс для расчета эмпирической дисперсии.
     */
    private final EmpiricalVarianceCalculator empiricalVarianceCalculator =
        new EmpiricalVarianceCalculator();

    /**
     * Рассчитать эксцесс.
     *
     * @param intervals - интервалы
     * @return эксцесс
     */
    public BigDecimal calculateIntervalExcess(List<Interval> intervals) {
        BigDecimal fourthCentralMoment = calculateFourthCentralMoment(intervals);
        return fourthCentralMoment.divide(empiricalVarianceCalculator
            .calculateEmpiricalVariance(intervals).pow(4), 10, RoundingMode.HALF_UP)
            .subtract(BigDecimal.valueOf(3));
    }

    /**
     * Рассчитать третий центральный момент.
     * @param intervals - интервалы
     * @return асимметрия
     */
    private BigDecimal calculateFourthCentralMoment(List<Interval> intervals) {
        BigDecimal average = intervalAverageCalculator.calculateIntervalAverage(intervals);
        BigDecimal numerator = BigDecimal.ZERO;

        for (Interval interval : intervals) {
            BigDecimal difference = interval.getMiddle().subtract(average);
            numerator = numerator.add(difference.pow(4)
                .multiply(BigDecimal.valueOf(interval.getFrequency())));
        }

        return numerator.divide(BigDecimal.valueOf(intervals.stream().mapToInt(Interval::getFrequency).sum()),
            10, RoundingMode.HALF_UP);
    }
}

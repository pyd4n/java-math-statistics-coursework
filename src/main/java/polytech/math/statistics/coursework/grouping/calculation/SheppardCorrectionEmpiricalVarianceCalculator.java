package polytech.math.statistics.coursework.grouping.calculation;

import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Расчет эмпирическую дисперсию с учетом смещения с помощью
 * поправки Шеппарда.
 */
public class SheppardCorrectionEmpiricalVarianceCalculator {
    /**
     * Класс для расчета эмпирической дисперсии.
     */
    private final EmpiricalVarianceCalculator empiricalVarianceCalculator =
        new EmpiricalVarianceCalculator();

    /**
     * Рассчитать эмпирическую дисперсию с учетом смещения с помощью
     * поправки Шеппарда.
     *
     * @param intervals - интервалы
     * @return значение эмпирической дисперсию с учетом смещения с помощью
     * поправки Шеппарда
     */
    public BigDecimal calculateEmpiricalVarianceWithCorrection(List<Interval> intervals) {
        BigDecimal empiricalVariance = empiricalVarianceCalculator
            .calculateEmpiricalVariance(intervals);

        BigDecimal intervalWidth = intervals.get(0).getEnd()
            .subtract(intervals.get(0).getStart());

        return empiricalVariance.pow(2).subtract(intervalWidth.pow(2)
            .divide(BigDecimal.valueOf(11), 10, RoundingMode.HALF_UP));
    }
}

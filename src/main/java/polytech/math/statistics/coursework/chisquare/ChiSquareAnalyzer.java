package polytech.math.statistics.coursework.chisquare;

import java.math.BigDecimal;
import java.util.List;

public class ChiSquareAnalyzer {
    static private final List<List<BigDecimal>> psiTable = List.of(
        // 1%  5%  10%
        List.of(BigDecimal.valueOf(6.63490), BigDecimal.valueOf(3.84146), BigDecimal.valueOf(2.70554)),
        List.of(BigDecimal.valueOf(9.21034), BigDecimal.valueOf(5.99146), BigDecimal.valueOf(4.60517)),
        List.of(BigDecimal.valueOf(11.34487), BigDecimal.valueOf(7.81473), BigDecimal.valueOf(6.25139)),
        List.of(BigDecimal.valueOf(13.2767), BigDecimal.valueOf(9.48773), BigDecimal.valueOf(7.77944)),
        List.of(BigDecimal.valueOf(15.08627), BigDecimal.valueOf(11.0705), BigDecimal.valueOf(9.23636)),
        List.of(BigDecimal.valueOf(16.81189), BigDecimal.valueOf(12.59159), BigDecimal.valueOf(10.64464)),
        List.of(BigDecimal.valueOf(18.47531), BigDecimal.valueOf(14.06714), BigDecimal.valueOf(12.01704)),
        List.of(BigDecimal.valueOf(20.09024), BigDecimal.valueOf(15.50731), BigDecimal.valueOf(13.36157)),
        List.of(BigDecimal.valueOf(21.66599), BigDecimal.valueOf(16.91898), BigDecimal.valueOf(14.68366)), 
        List.of(BigDecimal.valueOf(23.20925), BigDecimal.valueOf(18.30704), BigDecimal.valueOf(15.98718)),
        List.of(BigDecimal.valueOf(24.72497), BigDecimal.valueOf(19.67514), BigDecimal.valueOf(17.27501)),
        List.of(BigDecimal.valueOf(26.21697), BigDecimal.valueOf(21.02607), BigDecimal.valueOf(18.54935)), 
        List.of(BigDecimal.valueOf(27.68825), BigDecimal.valueOf(22.36203), BigDecimal.valueOf(19.81193)), 
        List.of(BigDecimal.valueOf(29.14124), BigDecimal.valueOf(23.68479), BigDecimal.valueOf(21.06414)),
        List.of(BigDecimal.valueOf(30.57791), BigDecimal.valueOf(24.99579), BigDecimal.valueOf(22.30713)), 
        List.of(BigDecimal.valueOf(31.99993), BigDecimal.valueOf(26.29623), BigDecimal.valueOf(23.54183)),
        List.of(BigDecimal.valueOf(33.40866), BigDecimal.valueOf(27.58711), BigDecimal.valueOf(24.76904)),
        List.of(BigDecimal.valueOf(34.80531), BigDecimal.valueOf(28.86930), BigDecimal.valueOf(25.98942)), 
        List.of(BigDecimal.valueOf(36.19087), BigDecimal.valueOf(30.14353), BigDecimal.valueOf(27.20357)), 
        List.of(BigDecimal.valueOf(37.56623), BigDecimal.valueOf(31.41043), BigDecimal.valueOf(28.41198))
    );
    /**
     * 
     * @param psiSquare - рассчитанный с помощью ChiSquareCalculator Хи Квадрат
     * @param k         - степень свободы, данного Хи Квадрата. Тоже считается с
     *                  помощью ChiSquareCalculator
     * @param q - уровень значимости. В долях, то есть процент/100.
     * @return True если данный хи квадрат не проходит по таблице и противоречит. 
     * False в остальных случаях.
     */
    public boolean isPsiSquareContraveneNormDistribution(BigDecimal psiSquare, Integer k, BigDecimal q) {
        return psiSquare.compareTo(getTablePsi(k, q)) >= 0;
    }

    public BigDecimal getTablePsi(Integer k, BigDecimal q) {
        var row = psiTable.get(k);
        if (q.compareTo(BigDecimal.valueOf(0.01)) == 0)
            return row.get(0);
        if (q.compareTo(BigDecimal.valueOf(0.05)) == 0)
            return row.get(1);
        if (q.compareTo(BigDecimal.valueOf(0.1)) == 0)
            return row.get(2);
        return BigDecimal.valueOf(-1);
    }
}

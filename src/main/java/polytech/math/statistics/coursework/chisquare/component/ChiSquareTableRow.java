package polytech.math.statistics.coursework.chisquare.component;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import polytech.math.statistics.coursework.grouping.component.Interval;

@Data
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class ChiSquareTableRow {
    /**
     * Интервал связанный с этой строчкой
     */
    private final Interval interval;

    /**
     * На сколько долей дисперсии смещен правый конец интервала
     */
    private final BigDecimal deviationFraction;

    /**
     * Вероятность попадания значения в заданный интервал
     */
    private final BigDecimal probability;

    /**
     * Математическое ожидание текущего интервала
     */
    private final BigDecimal mathExpectance;

    /**
     * Уклонение
     */
    private final BigDecimal frequencyExpectanceDifference;

    /**
     * Хи квадрат текущего интервала
     */
    private final BigDecimal psiSquare;

    /**
     *  Данный метод является вспомогательным средством объединения строк, мат ожидания
     *      которых меньше пяти
     * @apiNote Интервал правой строки должен быть продолжением интервала текущей строки
     */
    public ChiSquareTableRow unite(ChiSquareTableRow rhs) {
        ChiSquareTableRowBuilder builder = new ChiSquareTableRowBuilder();
        var newInterval = uniteIntervals(interval, rhs.interval);
        builder.withInterval(newInterval);
        builder.withProbability(probability.add(rhs.probability));
        var newExpectance = mathExpectance.add(rhs.mathExpectance);
        builder.withMathExpectance(newExpectance);
        builder.withDeviationFraction(rhs.deviationFraction);
        var frequencyExpectanceDifference = BigDecimal.valueOf(newInterval.getFrequency()).subtract(newExpectance);
        builder.withFrequencyExpectanceDifference(frequencyExpectanceDifference);
        var psiSquare = frequencyExpectanceDifference.multiply(frequencyExpectanceDifference)
                    .divide(newExpectance, 8, RoundingMode.HALF_UP);
        builder.withPsiSquare(psiSquare);
        return builder.build();
    }

    /**
     *
     * @apiNote Значение моды становится неопределенным у полученного интервала
     * @return Объеденённый интервал
     */
    private Interval uniteIntervals(Interval a, Interval b) {
        if (a.getStart().compareTo(b.getStart()) > 0) {
            var temp = a;
            a = b;
            b = temp;
        }
        var result = Interval.builder()
                .withStart(a.getStart())
                .withEnd(b.getEnd())
                .withFrequency(a.getFrequency() + b.getFrequency())
                .withIncidence(a.getIncidence().add(b.getIncidence()))
                .withMiddle(a.getMiddle().add(b.getMiddle()).divide(BigDecimal.valueOf(2), 8, RoundingMode.HALF_UP))
                .withAccumulatedIncidence(b.getAccumulatedIncidence())
                .build();
        return result;
    }

    @Override
    public String toString() {
        var intervalBeginStr = String.valueOf(interval.getStart());
        var intervalEndStr = String.valueOf(interval.getEnd());
        var deviationFractionStr = String.valueOf(deviationFraction.setScale(2, RoundingMode.HALF_UP));
        var probabilityStr = String.valueOf(probability.setScale(3, RoundingMode.HALF_UP));
        var mathExpectanceStr = String.valueOf(mathExpectance.setScale(3, RoundingMode.HALF_UP));
        var frequencyStr = String.valueOf(interval.getFrequency());
        var frequencyExpectanceDifferenceStr = String.valueOf(frequencyExpectanceDifference.setScale(4, RoundingMode.HALF_UP));
        var psiSquareStr = String.valueOf(psiSquare.setScale(4, RoundingMode.HALF_UP));
        return String.format("%7s%15s%15s%15s%15s%15s%15s%15s", 
                    intervalBeginStr, intervalEndStr, deviationFractionStr,
                    probabilityStr, mathExpectanceStr, frequencyStr,
                    frequencyExpectanceDifferenceStr, psiSquareStr);
    }
}

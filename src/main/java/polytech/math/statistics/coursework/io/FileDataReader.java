package polytech.math.statistics.coursework.io;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Класс для чтения данных из файла.
 */
public class FileDataReader {
    /**
     * Прочитать числа из файла.
     *
     * @param fileName - относительный путь до файла из папки resources
     * @return список прочитанных BigDecimal чисел
     */
    public List<NumberedValue> readNumbers(String fileName) {
        try (var reader = new BufferedReader(new InputStreamReader(
            Objects.requireNonNull(getClass().getClassLoader()
                .getResourceAsStream(fileName))
        ))) {
            List<NumberedValue> numbers = new ArrayList<>();
            reader.lines().forEach(line -> numbers.add(NumberedValue.builder()
                .withRowNumber(Integer.parseInt(line.split(" ")[0]))
                .withValue(BigDecimal.valueOf(Double
                    .parseDouble(line.split(" ")[1])))
                .build()));

            return numbers;
        } catch (Exception e) {
            throw new IllegalArgumentException("Ошибка при чтении файла: "
                + fileName);
        }
    }
}

package polytech.math.statistics.coursework.io;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Пронумерованная строка со значением.
 */
@Data
@Builder(setterPrefix = "with")
@AllArgsConstructor
public class NumberedValue {
    /**
     * Номер строки.
     */
    private final Integer rowNumber;

    /**
     * Значение строки.
     */
    private final BigDecimal value;
}

package polytech.math.statistics.coursework.trusting;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для вычисления доверительного интервала для
 * дисперсии и среднего квадратического отклонения.
 */
public class DispersionTrustedIntervalCalculator {
    /**
     * Класс для расчета доверительного интервала
     * для математического ожидания.
     */
    private final MathExpectationTrustIntervalCalculator mathExpectationTrustIntervalCalculator =
        new MathExpectationTrustIntervalCalculator();

    /**
     * Рассчитать левую границу для параметра сигма-квадрат.
     *
     * @param numbers - выборка чисел
     * @param x2      - хи два в квадрате
     * @return левая граница параметра сигма-квадрат
     */
    public BigDecimal calculateSquaredLeftBorder(List<BigDecimal> numbers, BigDecimal x2) {
        return BigDecimal.valueOf(numbers.size())
            .multiply(mathExpectationTrustIntervalCalculator.calculateDispersion(numbers).pow(2))
            .divide(x2, 10, RoundingMode.HALF_UP);
    }

    /**
     * Рассчитать левую границу для параметра сигма.
     *
     * @param numbers - выборка чисел
     * @param x2      - хи два в квадрате
     * @return левая граница параметра сигма
     */
    public BigDecimal calculateLeftBorder(List<BigDecimal> numbers, BigDecimal x2) {
        return calculateSquaredLeftBorder(numbers, x2).sqrt(MathContext.DECIMAL128)
            .setScale(10, RoundingMode.HALF_UP);
    }

    /**
     * Рассчитать правую границу для параметра сигма-квадрат.
     *
     * @param numbers - выборка чисел
     * @param x1      - хи один в квадрате
     * @return правая граница параметра сигма-квадрат
     */
    public BigDecimal calculateSquaredRightBorder(List<BigDecimal> numbers, BigDecimal x1) {
        return BigDecimal.valueOf(numbers.size())
            .multiply(mathExpectationTrustIntervalCalculator.calculateDispersion(numbers).pow(2))
            .divide(x1, 10, RoundingMode.HALF_UP);
    }

    /**
     * Рассчитать правую границу для параметра сигма.
     *
     * @param numbers - выборка чисел
     * @param x1      - хи один в квадрате
     * @return правая граница параметра сигма
     */
    public BigDecimal calculateRightBorder(List<BigDecimal> numbers, BigDecimal x1) {
        return calculateSquaredRightBorder(numbers, x1).sqrt(MathContext.DECIMAL128)
            .setScale(10, RoundingMode.HALF_UP);
    }
}
